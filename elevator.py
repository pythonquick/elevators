#!/usr/bin/env python
import sys


class State(object):
    '''State class that represents the positions of elevators at a given time'''
    def __init__(self, time):
        self.time = time
        self.floors = []

    def add_floor(self, floor_line):
        floor = floor_line.split('x')[1:-1]
        self.floors.insert(0, floor)

    def find_floor_with_elevator(self, elevator):
        for floor in self.floors:
            if elevator in floor:
                return floor
        raise ValueError('Could not find elevator %s in elevator state at time %d' % (elevator, self.time))

    def level_for_elevator(self, elevator):
        for floor_index, floor in enumerate(self.floors):
            if elevator in floor:
                return floor_index + 1
        raise ValueError('Could not find level for elevator %s in elevator state at time %d' % (elevator, self.time))

    def next_moves(self, elevator):
        floor = self.find_floor_with_elevator(elevator)
        return [slot for slot in floor if slot != '.']

    def __repr__(self):
        '''function to pretty-print State instances, useful during debugging'''
        return self.__str__()

    def __str__(self):
        '''function to pretty-print State instances, useful during debugging'''
        floors = self.floors[:] # copy the array
        floors.reverse() # reverse, so that lower-index floors appear at bottom
        floors_string = '\n'.join([str(floor) for floor in floors])
        return 'State-%d' % self.time + '\n' + floors_string + '\n\n'


def is_ceiling(line):
    '''Determine if the given line is a ceiling (top boundary) of a building)
        A ceiling line consists of all x'es'''
    return len(line) > 0 and line == 'x' * len(line)


def read_states(input_file_name):
    '''Return a list with State instances for all states in given text file'''
    time = 0
    states = []
    state = None
    f = open(input_file_name, 'rt')
    for line in f:
        line = line.strip()
        if is_ceiling(line):
            if state:
                states.append(state)
            time += 1
            state = State(time)
        elif len(line) == 0:
            pass
        elif len(line) > 0:
            state.add_floor(line)
    if state:
        states.append(state)

    f.close()
    return states


def explore_paths(current_time, current_elevator, path_so_far=[]):
    '''Attempt to find a path to target floor at target time.
        Note: this function depends and assumes existence of three global variables:
            1) states (list of State instances)
            2) target_floor (integer floor index, 1-based)
            3) target_time (integer time index, 1-based)'''
    state = states[current_time-1]
    current_floor = state.level_for_elevator(current_elevator)
    if current_time == target_time and current_floor == target_floor:
        return path_so_far
    if current_time < len(states):
        next_moves = state.next_moves(current_elevator)
        for next_move_elevator in next_moves:
            path = path_so_far[:] # copy the original list
            path.append(next_move_elevator)
            possible_solution = explore_paths(current_time + 1, next_move_elevator, path)
            if possible_solution:
                return possible_solution
    return None


states = None
target_floor = None
target_time = None


if __name__ == '__main__':
    args = sys.argv[1:]
    #args = ['states-file.txt', 'A', '1-3']
    if len(args) < 3:
        sys.exit('''Specify 3 parameters: <elevator system filename> <starting elevator> <final destination>
            for example:
            $ ./elevator.py states-file.txt A 1-3''')
            
    input_file_name = args[0]
    states = read_states(input_file_name)
    starting_elevator = args[1]
    final_destination = args[2]

    if not starting_elevator in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
        sys.exit('Second parameter must be starting elevator: an uppercase character from A to Z')

    destination_components = final_destination.split('-')
    if len(destination_components) != 2:
        sys.exit('Third parameter <final destination> must contain two integer values separated by "-"')

    try:
        target_floor = int(destination_components[0])
        target_time = int(destination_components[1])
    except ValueError as err:
        sys.exit('Third parameter <final destination> must contain two integer values separated by "-"')

    if target_time > len(states):
        sys.exit('<final destionation> specifies the time %d but there are only %d elevator time states in the input file' % (target_time, len(states)))

    # Try to find a solution path starting at elevator state 1, ending at specified final destination
    # Then print the solution path to std out, or a message to std err if no path found:
    solution_path = explore_paths(1, starting_elevator)
    if solution_path:
        print ''.join(solution_path)
    else:
        sys.stderr.write('There is no path for given start elevator and final destination\n')
