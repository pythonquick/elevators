# Elevator
Please write a program that satisfies the following spec and email back the source code for it once you are done.

You can ask questions along the way.

Feel free to use the language that you work best in, such as Scala, Java, Python, JavaScript, or Ruby. If your solution is not in one of these languages please include instructions for how to run it.

We look for candidates to finish in 3 hours. If you reach 4 hours, please finish what you're working on and send in whatever code you have.

Priorities:
 1. Correctness *(hint: verify your program works with the example command given below!)*
 2. Implementation time
 3. Elegance of code
 4. Anything else you think we might care about in a coding problem.

The following diagram is an elevator state--- the state of an elevator system at a given point in time. Dots represent an elevator shaft. Captial letters represent an elevator. For example, elevator A is on the 4th floor (1-indexed), and the following elevator state has 4 floors:

```
xxxxxxxxxxx
xAxBx.x.x.x
x.x.x.xDx.x
x.x.x.x.xEx
x.x.xCx.x.x
```

Input: Take a series of elevator states from a file, representing successive states of the elevator system from t = {1,2,3...} Each elevator state will be separated by an empty line. Valid elevators are specified by the characters A-Z.

An elevator system with elevator states for t=1, 2, and 3 might look like:

```
xxxxxxxxxxx
xAxBx.x.x.x
x.x.x.xDx.x
x.x.x.x.xEx
x.x.xCx.x.x

xxxxxxxxxxx
xAx.x.x.x.x
x.xBxCx.xEx
x.x.x.x.x.x
x.x.x.xDx.x

xxxxxxxxxxx
xAxBx.x.x.x
x.x.x.xDx.x
x.x.xCx.x.x
x.x.x.x.xEx
```

Your challenge is to write a command line program that finds a series of legal actions that lead you from a starting elevator to a desired floor in a specified amount of time.

```
<yourProgramExecutable> <elevator system filename> <starting elevator> <final destination>
```

Where `<final destination>` is specified as `<floor>-<time>`, e.g. 1-3, indicating that the final destination is the first floor at time t=3.

At t=1, you begin in the elevator specified by `<starting elevator>`, e.g. "A".

In a given time period, you can board an elevator or stay on the elevator you are on. In any time period, you can only board an elevator on the same floor as the elevator that you are currently on (including t=1).

The set of actions should be written to stdout as a series of actions in a single string. An action is defined as the elevator you board (or stay on) at time t.

Given the example elevator states above, your program should work as follows:

```
$ elevator-executable states-file.txt A 1-3
BE
```

The output is `BE` because at time 1 you switch from elevator A to elevator B, and then at time 2 you switch from elevator B to elevator E, and then elevator E arrives at the desired floor 1 at time 3. 

So to generalize, if the final destination time is T, your actions should have T-1 actions in it (the elevator you are on at time T-1 must transition to the correct floor at time T for your solution to be valid). The elevator at time T should *not* be in the solution. Similarly, the start elevator is only in the solution if you opted to stay on it at t=1.

If there is a solution, the solution string is the *only* thing that should be printed to stdout.

If there is no solution, print something appropriate to stderr, and *nothing* to stdout.
